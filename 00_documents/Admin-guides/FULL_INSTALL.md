This document describes how to:

+ Prepare the configuration with the [Preconfiguration](#pre-setup-steps)
+ install [MicroK8s](#install-microk8s) on a Linux server, running Debian or Ubuntu .
+ Deploy the full platform installation
+ 
## Prerequisites
- [git](https://git-scm.com)
- a SSH key pair to use.
- IP address of the Linux server you want to install [MicroK8s](https://microk8s.io).
- Credentials to access the Linux server.
- An email address for [cert-manager](https://cert-manager.io)

## Pre-Setup Steps

In Order to be able to run the complete installation, one central Ansible Inventory File has to be prepared. 

Additionally some Environment Variables have to be set.

### Configure Inventory File

First create a copy based on the template-

```
# Copy template
cp inventory.default inventory

# Edit and set right parameters
vim inventory
```

The File `inventory` containes the following parameters to set:

```ini
[k8s_cluster]
k8s-master ansible_host="<ip_address>"

[localhost]
127.0.0.1   ansible_connection=local

[localhost:vars]
ansible_python_interpreter="{{ ansible_playbook_python }}"

[k8s_cluster:vars]
ansible_python_interpreter=/usr/bin/python3
ansible_password=<initial_root_password>

[all:vars]
# Common Data
kubeconfig_file=<name_for_local_kubeconfig_file>
working_user=<name_for_localhost_user>
working_group=<name_for_localhost_group>
```

After setting all parameters accordingly, save the file.

## Install MicroK8s

Run the following command from Repo Root Folder to preconfigure the VM.

```
ansible-playbook -i inventory -l k8s-master -u root 01_base-cluster/setup_k8s_playbook_01.yml
```

Next start the installation of MicroK8s with the next two commands:

```
ansible-playbook -i inventory -l k8s-master -u acn --private-key ~/.ssh/<private_kay> 01_base-cluster/setup_k8s_playbook_02.yml

ansible-playbook -i inventory -l localhost 01_base-cluster/setup_k8s_playbook_03.yml

```

Now you have a running Installation of MicroK8s and a kubeconfig file in you .kube folder of your home directory.
